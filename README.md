# 事前準備
dockerがインストールされていること。

# 環境準備
    # 管理者権限に昇格
    sudo -i

    # Dockerfileでイメージを作成
    docker build -t jenkins-master ./

    # 作成したイメージで起動（ポート番号はオフィシャルのjenkinsのままで設定）
    # jenkinsそのもののユーザ情報やジョブの管理は問題ないはずなので永続化しておく。
	# --cap-add=SYS_ADMINしないとchromeのヘッドレスモードを起動することができない。
    docker run --cap-add=SYS_ADMIN -p 8080:8080 -p 50000:50000 -v `pwd`/jenkins_home:/var/jenkins_home jenkins-master

# 注意事項
以下にあるドライバーはcloneしたまま**使えない**ので、公式ページからダウンロードして**入れ替える**必要あり。

ちなみに、Windowsのドライバーはcloneしたままで問題ない。

driver/chromedriver_linux64/chromedriver

Jenkinsでクローン後に以下を実行する想定。
    cd driver/chromedriver_linux64
    rm *
    curl -O https://chromedriver.storage.googleapis.com/2.34/chromedriver_linux64.zip
    unzip chromedriver_linux64.zip
    cd ../..
    sh gradlew test