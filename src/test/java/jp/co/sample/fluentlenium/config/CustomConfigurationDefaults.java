package jp.co.sample.fluentlenium.config;

import org.fluentlenium.configuration.ConfigurationDefaults;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class CustomConfigurationDefaults extends ConfigurationDefaults {
    private static final String OS_NAME = System.getProperty("os.name").toLowerCase();

    @Override
    public String getWebDriver() {

        String path = "driver/chromedriver_win32/chromedriver.exe";
        if (OS_NAME.startsWith("linux")) {
            System.out.println("##################");
            System.out.println(System.getProperty("user.dir"));
            System.out.println("##################");
            path = "driver/chromedriver_linux64/chromedriver";
        }

        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/" + path);

        return "chrome";
    }

    @Override
    public Capabilities getCapabilities() {
        ChromeOptions options = new ChromeOptions();
        //options.addArguments("--no-sandbox");
        //options.addArguments("--headless");
        //options.addArguments("--disable-gpu");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("chromeOptions", options);
        System.out.println("##################");
        return capabilities;
    }

}
