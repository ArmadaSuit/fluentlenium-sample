package jp.co.sample.fluentlenium;

import static org.assertj.core.api.Assertions.*;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.Test;

@Wait
public class DuckDuckGoTest extends FluentTest {
    @Test
    public void titleOfDuckDuckGoShouldContainSearchQueryName() {
        goTo("https://duckduckgo.com");
        $("#search_form_input_homepage").fill().with("FluentLenium");
        $("#search_button_homepage").submit();
        await().atMost(5, TimeUnit.SECONDS).until(el("#search_form_homepage")).not().present();
        takeScreenShot("build/screenshot/"+ new Object(){}.getClass().getEnclosingMethod().getName()+"/" + new Date().getTime() + ".png");
        assertThat(window().title()).contains("FluentLenium");
    }
}
