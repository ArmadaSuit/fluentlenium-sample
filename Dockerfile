FROM jenkins

USER root

# libappindicator1はchromeのインストールで必要。
# unzipは後でchromedriverをインストールする時に必要。
RUN apt-get update && apt-get install -y libappindicator1 unzip

# chromeのインストール
RUN curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

RUN dpkg -i google-chrome-stable_current_amd64.deb || \
  (apt-get -f install -y && \
      dpkg -i google-chrome-stable_current_amd64.deb)

RUN rm -f google-chrome-stable_current_amd64.deb

USER jenkins
